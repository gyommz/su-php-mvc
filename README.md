# Task Manager

This project is a task manager developed in object-oriented PHP using the MVC model (Model-View-Controller). It allows users to manage their daily tasks, allowing them to register, login, and add/delete/edit their tasks.

## Features

- User Registration: Users can create an account by providing their basic information.
- User Login: Users can login to their existing account.
- Adding tasks: logged in users can add new tasks with title, description, due date and status.
- Editing Tasks: Users can edit the details of an existing task.
- Delete Tasks: Users can delete tasks.
- Tasks view: users can see the list of their tasks in a dashboard.

## Project Structure

The project is organized according to the MVC model:

- The `models` folder contains the model classes, including the `User` class to manage users and the `Task` class to manage tasks.
- `views` folder contains view files, such as `signup.php` for signup, `login.php` for login, `dashboard.php` for dashboard, `add_task.php` for the add task form, and `edit_task.php` for the edit task form.
- `controllers` folder contains controller classes including `UserController` to handle user related operations and `TaskController` to handle task related operations.
- The `database` folder contains a database configuration file, which can be modified according to your MySQL database settings.

## Prerequisites

- Ports 8000 and 8080 available
- Docker: [Install Docker](https://docs.docker.com/get-docker/)
- Make sure you have [Composer](https://getcomposer.org/) installed on your system.


## Setup

Before you begin, you need to set up your environment by following these steps:

1. Create a `.env.local` file at the root of the project.

2. In the `.env.local` file, set the following environment variables:
```bash
DB_HOST=mysql
DB_PORT=3306
DB_NAME=gestionnaire_taches
DB_CHARSET=utf8mb4
DB_USER=root
DB_PASSWORD=mot_de_passe_de_la_base_de_données
```
3. Run the following command at the root of the project to install the dependencies:

```bash
composer install
```


## Install and run with Docker

1. Clone or download this repository in the directory of your choice.

2. Navigate to the project directory:

    ```bash
    cd gestionnaire-taches
    ```
3. Update the database connection information in the database/config.php file.

4. Start Docker containers by running the following command:

    ```bash
    docker compose up -d
    ```

5. Go to [app](http://localhost:8000)

6. You can also access [phpMyAdmin](http://localhost:8080)

**Note:** When starting the app, sample data will be generated automatically to test functionality.


## Tests

This project uses PHPUnit as a testing framework for unit testing. Make sure you have installed the development dependencies by running the `composer install` command.

To run the unit & integration tests, use the following command:

```bash
composer test
```
## Additional notes

- This project is provided as an example and does not guarantee complete security. Be sure to implement additional security mechanisms before deploying the application to a production environment.

- This project was made as part of a school project and can be used as a starting point to develop similar applications. Feel free to explore and make improvements to this project based on your specific needs.
