<?php

use Symfony\Component\Dotenv\Dotenv;

require_once '../vendor/autoload.php';
require_once __DIR__ . '/../src/Routing/Router.php';

// use this for debugging
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

function loadEnvironmentVariables(string $path): void
{
    $dotenv = new Dotenv();
    $dotenv->load($path);
}

function createDatabaseConnection(array $config): PDO
{
    try {
        $dsn = "mysql:host={$config['DB_HOST']};port={$config['DB_PORT']};dbname={$config['DB_NAME']};charset={$config['DB_CHARSET']}";
        return new PDO($dsn, $config['DB_USER'], $config['DB_PASSWORD']);
    } catch (PDOException $e) {
        die("Erreur de connexion à la base de données : " . $e->getMessage());
    }
}

loadEnvironmentVariables(__DIR__ . '/../.env');

$pdo = createDatabaseConnection($_ENV);


$router = new \App\Routing\Router();
$controllerFiles = glob(__DIR__ . '/../src/Controllers/*.php');
$router->generateRoutes($controllerFiles);
$router->handleRequest($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

