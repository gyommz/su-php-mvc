USE gestionnaire_taches;

CREATE TABLE users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(255) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users (username, email, password) VALUES
('john_doe', 'john@example.com', 'password1'),
('jane_smith', 'jane@example.com', 'password2');

CREATE TABLE tasks (
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  title VARCHAR(100) NOT NULL,
  description TEXT,
  due_date DATE NOT NULL,
  status ENUM('todo', 'done') NOT NULL DEFAULT 'todo',
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tasks (user_id, title, description, due_date) VALUES
(1, 'Faire les courses', 'Acheter du lait, des œufs et du pain.', '2023-06-15'),
(1, 'Répondre aux e-mails', 'Répondre aux e-mails en attente.', '2023-06-10'),
(2, 'Préparer la présentation', 'Préparer les diapositives pour la réunion.', '2023-06-12');
