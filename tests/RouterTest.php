<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use App\Routing\Router;

class RouterTest extends TestCase
{
  public function testAddRoute()
  {
    $router = new Router();
    $method = 'GET';
    $path = '/example';
    $callback = function () {
      return 'Hello, World!';
    };

    $router->addRoute($method, $path, $callback);

    $routes = $router->getRoutes();
    $this->assertCount(1, $routes);

    $route = $routes[0];
    $this->assertEquals($method, $route['method']);
    $this->assertEquals($path, $route['path']);
    $this->assertInstanceOf(\Closure::class, $route['callback']);
  }

  // public function testGenerateRoutes()
  // {
  //   $mockedRouter = $this->getMockBuilder(Router::class)
  //     ->setMethods(['getNamespacePrefix'])
  //     ->getMock();

  //   $mockedRouter->expects($this->any())
  //     ->method('getNamespacePrefix')
  //     ->willReturn('tests\\Controllers\\');

  //   $controllerFiles = glob(realpath(__DIR__ . '/../tests/Controllers') . '/*.php');

  //   $mockedRouter->generateRoutes($controllerFiles);

  //   $routes = $mockedRouter->getRoutes();
  //   $this->assertCount(1, $routes['GET:/test']);
  //   $this->assertArrayHasKey('GET:/test', $routes);
  //   $this->assertEquals('tests\Controllers\TestController', $routes['GET:/test'][0]);
  // }

  // public function testHandleRequest()
  // {
  //   $router = new Router();

  //   // Ajouter des routes
  //   $router->addRoute('GET', '/test', function () {
  //     echo 'This is a test route';
  //   });

  //   // Test d'une route existante
  //   ob_start();
  //   $router->handleRequest('GET', '/test');
  //   $output = ob_get_clean();
  //   $this->assertEquals('This is a test route', $output);

  //   // Test d'une route inexistante
  //   $this->expectException(RouteNotFoundException::class);
  //   $router->handleRequest('GET', '/non-existing');
  // }



  public function testHandleRequestWithNonExistingRoute()
  {
    $router = new Router();

    $this->expectException(\App\Routing\RouteNotFoundException::class);
    $this->expectExceptionMessage('Route not found');

    $router->handleRequest('GET', '/non-existing');
  }



  public function testGetControllerClassName()
  {
    $router = new Router();

    $file = __DIR__ .'/src/Controllers/TestController.php';
    $className = $router->getControllerClassName($file);

    $this->assertEquals('App\Controllers\TestController', $className);
  }

  public function testGetControllerMethods()
  {
    $router = new Router();

    $className = 'App\Controllers\TestController';
    $methods = $router->getControllerMethods($className);

    $this->assertNotEmpty($methods);
    $this->assertInstanceOf(\ReflectionMethod::class, $methods[0]);
    $this->assertTrue($methods[0]->isPublic());
  }

  public function testHasRouteAnnotation()
  {
    $router = new Router();

    $methodWithAnnotation = new \ReflectionMethod('App\Controllers\TestController', 'index');
    $hasAnnotation = $router->hasRouteAnnotation($methodWithAnnotation);
    $this->assertEquals(1, $hasAnnotation);

    $methodWithoutAnnotation = new \ReflectionMethod('App\Controllers\TestController', 'someMethod');
    $hasNoAnnotation = $router->hasRouteAnnotation($methodWithoutAnnotation);
    $this->assertEquals(0, $hasNoAnnotation);
  }

  public function testExtractRouteOptions()
  {
    $router = new Router();

    $annotation = '/** @route(method="GET", path="/test") */';
    $options = $router->extractRouteOptions($annotation);

    $expectedOptions = [
      'method' => 'GET',
      'path' => '/test'
    ];

    $this->assertEquals($expectedOptions, $options);
  }

  public function testRegisterRoute()
  {
    $router = new Router();

    $router->registerRoute('GET', '/test', 'App\Controllers\TestController', 'index');

    $routes = $router->getRoutes();

    $this->assertCount(1, $routes);
    $this->assertEquals('GET', $routes[0]['method']);
    $this->assertEquals('/test', $routes[0]['path']);
    $this->assertEquals(['App\Controllers\TestController', 'index'], $routes[0]['callback']);
  }

}