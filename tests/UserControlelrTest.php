<?php

use PHPUnit\Framework\TestCase;
use App\Controllers\UserController;

class UserControllerTest extends TestCase
{
    public function testIndex()
    {
        $homeController = new UserController();
        
        ob_start();
        $homeController->index();
        $output = ob_get_clean();
        
        $expectedOutput = 'Welcome to the user page!';
        $this->assertEquals($expectedOutput, $output);
    }
}
