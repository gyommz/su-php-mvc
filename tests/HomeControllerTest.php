<?php

use PHPUnit\Framework\TestCase;
use App\Controllers\HomeController;

class HomeControllerTest extends TestCase
{
    public function testIndex()
    {
        $homeController = new HomeController();
        
        ob_start();
        $homeController->index();
        $output = ob_get_clean();
        
        $expectedOutput = 'Welcome to the home page!';
        $this->assertEquals($expectedOutput, $output);
    }
}
