<?php

namespace tests\Controllers;

class TestController
{
  /**
   * @route(method="GET", path="/test")
   */
  public function index()
  {
    echo 'Welcome to the test page!';
  }
}