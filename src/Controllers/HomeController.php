<?php

namespace App\Controllers;


class HomeController
{
    /**
     * @route(method="GET", path="/home")
     */
    public function index()
    {
        echo 'Welcome to the home page!';
    }
}
