<?php

namespace App\Controllers;


class UserController
{
    /**
     * @route(method="GET", path="/user")
     */
    public function index()
    {
        echo 'Welcome to the user page!';
    }
}
