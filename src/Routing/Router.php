<?php

namespace App\Routing;
use App\Routing\RouteNotFoundException;

class Router
{
  private $routes;

  public function __construct()
  {
    $this->routes = [];
  }

  public function addRoute(string $method, string $path, callable $callback)
  {
    $this->routes[] = [
      'method' => $method,
      'path' => $path,
      'callback' => $callback
    ];
  }

  public function getRoutes(): array
  {
    return $this->routes;
  }

  public function handleRequest(string $method, string $path)
  {
    foreach ($this->routes as $route) {
      if ($route['method'] === $method && $route['path'] === $path) {
        $callback = $route['callback'];

        if (is_array($callback)) {
          $className = $callback[0];
          $methodName = $callback[1];
          $controllerInstance = new $className();
          $controllerInstance->$methodName();
          return;
        }
      }
    }

    throw new RouteNotFoundException('Route not found');
  }


  public function generateRoutes(array $controllerFiles)
  {
    array_map([$this, 'loadControllerFile'], $controllerFiles);
    $controllersData = array_map([$this, 'getControllerClassName'], $controllerFiles);
    $methodsData = array_map([$this, 'getControllerMethods'], $controllersData);

    foreach ($methodsData as $methods) {
      foreach ($methods as $method) {
        if ($this->hasRouteAnnotation($method)) {
          $options = $this->extractRouteOptions($method->getDocComment());
          $routeMethod = $options['method'] ?? 'GET';
          $routePath = $options['path'] ?? '';

          $this->registerRoute($routeMethod, $routePath, $method->getDeclaringClass()->getName(), $method->getName());
        }
      }
    }
  }


  public function loadControllerFile($file)
  {
    $controllerFilePath = realpath($file);
    if ($controllerFilePath !== false) {
      require_once $controllerFilePath;
    }
  }

  public function getControllerClassName($file)
  {
    $className = basename($file, '.php');
    return 'App\\Controllers\\' . $className;
  }

  public function getControllerMethods($className)
  {
    $reflectionClass = new \ReflectionClass($className);
    return $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);
  }

  public function hasRouteAnnotation($method)
  {
    $annotation = $method->getDocComment();
    return preg_match('/@route\((.*?)\)/', $annotation, $matches);
  }

  public function extractRouteOptions($annotation)
  {
    $options = [];
    preg_match('/@route\((.*?)\)/', $annotation, $matches);

    if (!empty($matches[1])) {
      $optionsStr = str_replace(['"', "'"], '', $matches[1]);
      $optionsArr = explode(',', $optionsStr);

      foreach ($optionsArr as $option) {
        $parts = explode('=', $option);
        if (count($parts) === 2) {
          $key = trim($parts[0]);
          $value = trim($parts[1]);
          $options[$key] = $value;
        }
      }
    }

    return $options;
  }

  public function registerRoute($routeMethod, $routePath, $className, $methodName)
  {
    $this->addRoute($routeMethod, $routePath, [$className, $methodName]);
  }
}