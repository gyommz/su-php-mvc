<?php

namespace App\Routing;

use Exception;

class RouteNotFoundException extends Exception
{
  protected $message;
  protected $code;

  public function __construct($message = 'Route not found', $code = 404, Exception $previous = null)
  {
    $this->message = $message;
    $this->code = $code;
    parent::__construct($message, $code, $previous);
  }

  public function __toString()
  {
    return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  }
}